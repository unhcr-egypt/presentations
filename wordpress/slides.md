---
title: WordPress
verticalSeparator: <!--v-->
theme: unhcr/unhcr.css
revealOptions:
    transition: 'fade'
slideNumber: "c/t"
progress: true
---
# WordPress

### Content Management System (CMS)

### A quick overview

Vibek Raj Maurya // UNHCR Egypt
---

## Overview

- Website
- CMS
- Wordpress
- UNHCR Egypt Country Site

---

## Website

> a set of related **web pages** located under a single domain name, typically produced by a single person or organization.
> *Oxford Dictionary*

notes: documents that are accessed through the internet
---

## Why do we need website?
- Credibility<!-- .element: class="fragment appear" -->
- Control the narrative<!-- .element: class="fragment appear" -->
- Expand working hours <!-- .element: class="fragment appear" -->
- Showcase offers and services<!-- .element: class="fragment appear" -->
- Stakeholders expect it<!-- .element: class="fragment appear" -->

---
 ## So how to create a website?
---
### Do it a hard way - start coding

   <img width="800" src="images/html-sample-web-page.jpeg" style="border:0; text-decoration:none; outline:none">


---
### Do it an easy way - use CMS


   <img width="800" src="images/cms_general.jpeg" style="border:0; text-decoration:none; outline:none">

---
## What is CMS?

> Content Management System (CMS) is a **web application** that provides capabilities for **multiple users** with different permission level to manage web pages without coding knowledge

 <!--v-->
## What can CMS do?
- Create<!-- .element: class="fragment appear" -->
- Edit<!-- .element: class="fragment appear" -->
- Publish<!-- .element: class="fragment appear" -->
- Archive<!-- .element: class="fragment appear" -->
---

<img width="1024" src="images/cmscloud.jpeg" style="border:0; text-decoration:none; outline:none">

---
## Popular CMS

   <img width="550" src="images/3_cms.png" style="border:0; text-decoration:none; outline:none">

notes: Wordpress is best choice for beginners because of it's ease-of-use. Extensible. Joomla is great for e-commerce site, requires basic technical skills. Drupal, the most difficult among 3 but most powerful. Requires familiar understanding of HTML, CSS and PHP.
---
## Wordpress market share
  <img width="600" src="images/wordpress_share.png" style="border:0; text-decoration:none; outline:none">

---

## What is WordPress?
WordPress is an online, **open source** website creation tool written in PHP. In non-greek term, it is probably the easiest and the most powerful blogging and website content management system (cms) in existence today.

---

  <img width="600" src="images/wordpress_features.jpeg" style="border:0; text-decoration:none; outline:none">

notes: free, user friendly, easy to install, easy to customize, free themes, free plugins, community support, WYSIWYG

---
## Wordpress Terminology

__Dashboard__<!-- .element: class="fragment appear" --> 
_First screen in the administration area which displays the overview of the website_ <!-- .element: class="fragment appear" --> 

__Posts__ <!-- .element: class="fragment appear" --> 
_are blog content listed in a reverse chronological order (newest content on top)_<!-- .element: class="fragment appear" --> 

__Pages__ <!-- .element: class="fragment appear" --> 
_live outside of the normal blog chronology, and are often used to present timeless information about yourself or your site_ <!-- .element: class="fragment appear" -->

__Plugin__ <!-- .element: class="fragment appear" -->
_is a piece of software containing a group of functions that can be added to a WordPress website_ <!-- .element: class="fragment appear" --> 

notes: Pages can be hierarchical, which means a page can have sub pages, for example a parent page titled “About us” can have a sub-page called “Our history”. On the other hand posts are not hierarchical.https://www.wpbeginner.com/glossary/page/

---

## Wordpress Terminology
__Taxonomies__ <!-- .element: class="fragment appear" -->_are used as a way to group posts and custom post types together - Categories and Tags_<!-- .element: class="fragment appear" -->

__Category__ <!-- .element: class="fragment appear" -->_is one of the default taxonomies. Categories are used to sort and group  blog posts into different sections. eg. News & Stories, Press Release, Report_<!-- .element: class="fragment appear" -->

__Permalinks__<!-- .element: class="fragment appear" --> _are the permanent URLs of your individual blog post or page on the WordPress site_<!-- .element: class="fragment appear" -->

---

## Demo/Walk Through

---

### Dashboard

notes: Total posts, total pages. Posts  Media Pages

---

### Post

notes: 

---

### Pages

---

### DIVI

---

### Resources

UNHCR Web Editor Reference Docs
https://unhcr365.sharepoint.com/sites/der-webeditors/SitePages/How-.aspx


Divi Builder
https://www.youtube.com/watch?v=Kj07Ha0dhaI


Practice/Create your own Wordpress site
https://wordpress.com/

---

#### THANK YOU

---